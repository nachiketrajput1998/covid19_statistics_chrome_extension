# COVID-19 Statistics Chrome Extension

This browser extension helps to get the COVID-19 patients statistics in your district as per location of your system.<br>
After installation the extension icon will be automatically reflected on your browser's menu bar.<br>
Click on extension icon -> Click on Get Status button -> View statistics.
    
**Installation Guideline**

    Clone this repository on your system
    Open Chrome Extensions -> Customize & Control Google Chrome -> More Tools -> Extensions
    Turn on Developer Mode using toggle button if not already done. (Top Right cornor)
    Click on Load Unpacked -> This opens a filepicker, select the cloned folder.
    Done
    
**API's Used**
    
    Reverse Geolocation API 
    state-district-wise COVID-19 API
    
    
Open a issue for more feature or fixes.