var x = document.getElementById("demo");
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};

function success(pos) {
	var crd = pos.coords;
	var address;
	var state;
	var district;
	
	var all_india;
	var india_data;
	var india_active;
	var india_confirmed;
	var india_deaths;
	var india_recoverd; 

	var state_data;
	var state_name;
	var state_active;
	var state_confirmed;
	var state_deaths;
	var state_recoverd; 


 
	//Request for Reverse geolocation api
	
	var settings = {
	  "async": false,
  	  "crossDomain": true,
          "url": "https://us1.locationiq.com/v1/reverse.php?key=ceca49f6661413&lat=" +crd.latitude + "&lon=" + crd.longitude + "&format=json",
          "method": "GET"
	}

	$.ajax(settings).done(function (response) {
		address = response["address"]
		state = address["state"]
  		district = address["state_district"]
	});
	
	//Request for state and district covid-19 api	
  	url = 'https://api.covid19india.org/state_district_wise.json'

  	req=new XMLHttpRequest();
    	req.open("GET",url,true);
    	req.send();
	req.onload=function(){
        json=JSON.parse(req.responseText);
        result = json;  

        state1 = result[state]
        districtData = state1["districtData"]
        
        document.getElementById('demo').style.display = 'none';
        document.getElementById('demobr').style.display = 'none';
        
        document.getElementById('status').style.display = 'none';

        document.getElementById('stat').style.display = 'block';                                

        document.getElementById('district').style.display = 'block';                
        document.getElementById('district').innerHTML = district + " :" + '<br>';
        
        document.getElementById('state').style.display = 'block';                
        document.getElementById('state').innerHTML = '<br>' + state + " :" + '<br>';

        document.getElementById('country').style.display = 'block';                
        document.getElementById('country').innerHTML = '<br>' + "India" + " :" + '<br>';

	//Another request for state and district covid-19 api
	var data = null;

	var xhr = new XMLHttpRequest();
	xhr.withCredentials = true;
	
	xhr.addEventListener("readystatechange", function () {
		if (this.readyState === this.DONE) {
			all_india = JSON.parse(this.responseText);
			india_data = all_india["total_values"]
			state_data = all_india["state_wise"]
			state_name =  state_data[state]
			

			india_active = india_data["active"]
			india_confirmed = india_data["confirmed"]
			india_deaths = india_data["deaths"]
			india_recoverd = india_data["recovered"]	
			
			state_active = state_name["active"]
			state_confirmed = state_name["confirmed"]
			state_deaths = state_name["deaths"]
			state_recoverd = state_name["recovered"]

		
		//State
		//Active
	        document.getElementById('sl_active').style.display = 'block';                
	        document.getElementById('s_active').style.display = 'block';                
	        document.getElementById('s_active').innerHTML = state_active;
        
	        //Confirmed
	        document.getElementById('sl_confirmed').style.display = 'block';                
	        document.getElementById('s_confirmed').style.display = 'block';                
	        document.getElementById('s_confirmed').innerHTML = state_confirmed;
        
	        //Deceased
	        document.getElementById('sl_deceased').style.display = 'block';                
	        document.getElementById('s_deceased').style.display = 'block';                
	        document.getElementById('s_deceased').innerHTML = state_deaths;

		//Recovered
		document.getElementById('sl_recovered').style.display = 'block';                
	        document.getElementById('s_recovered').style.display = 'block';                
	        document.getElementById('s_recovered').innerHTML = state_recoverd;
	        	
		//India	
		//Active
	        document.getElementById('il_active').style.display = 'block';                
	        document.getElementById('i_active').style.display = 'block';                
	        document.getElementById('i_active').innerHTML = india_active;
        
	        //Confirmed
	        document.getElementById('il_confirmed').style.display = 'block';                
	        document.getElementById('i_confirmed').style.display = 'block';                
	        document.getElementById('i_confirmed').innerHTML = india_confirmed;
        
	        //Deceased
	        document.getElementById('il_deceased').style.display = 'block';                
	        document.getElementById('i_deceased').style.display = 'block';                
	        document.getElementById('i_deceased').innerHTML = india_deaths;

		//Recovered
		document.getElementById('il_recovered').style.display = 'block';                
	        document.getElementById('i_recovered').style.display = 'block';                
	        document.getElementById('i_recovered').innerHTML = india_recoverd;


		}
	});

	xhr.open("GET", "https://corona-virus-world-and-india-data.p.rapidapi.com/api_india");
	xhr.setRequestHeader("x-rapidapi-host", "corona-virus-world-and-india-data.p.rapidapi.com");
	xhr.setRequestHeader("x-rapidapi-key", "163f75109emsh4976a2aabf5a5acp1261c8jsn69839834154d");
	
	xhr.send(data);

                
       	fr = districtData[district]
        
	document.getElementById("division").style.width = "300px";
	document.getElementById("division").style.height = "350px";
	document.getElementById("division").style.overflowY = "scroll";
	
	//District	
	//Active
        document.getElementById('dl_active').style.display = 'block';                
        document.getElementById('d_active').style.display = 'block';                
        document.getElementById('d_active').innerHTML = JSON.stringify(fr["active"]);
        
        //Confirmed
        document.getElementById('dl_confirmed').style.display = 'block';                
        document.getElementById('d_confirmed').style.display = 'block';                
        document.getElementById('d_confirmed').innerHTML = JSON.stringify(fr["confirmed"]);
        
        //Deceased
        document.getElementById('dl_deceased').style.display = 'block';                
        document.getElementById('d_deceased').style.display = 'block';                
        document.getElementById('d_deceased').innerHTML = JSON.stringify(fr["deceased"]);

	//Recovered
	document.getElementById('dl_recovered').style.display = 'block';                
        document.getElementById('d_recovered').style.display = 'block';                
        document.getElementById('d_recovered').innerHTML = JSON.stringify(fr["recovered"]);
        
    };
}


function error(err) {
  alert('Check network connection!');
}
document.addEventListener('DOMContentLoaded', function() {
  var checkPageButton = document.getElementById('status');
  checkPageButton.addEventListener('click', function() {
	//alert('Hi')
	chrome.permissions.contains({
        permissions: ['geolocation'],
        origins: ['http://www.google.com/']
      }, function(result) {
        if (result) {
		navigator.geolocation.getCurrentPosition(success, error, options);
        } else {
          alert("The extension doesn't have the permissions")
        }
      });
  }, false);
}, false);

